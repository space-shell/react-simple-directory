import React from 'react'

const Subject = ({children, info, display}) => {
  return display
}

// Builds a prograss bar from cell data
const Progress = ({data, total = 100}) => {
  if(!Number(data)) return data
  data = Number(data)
  let perc = ((data/total)*100)
  perc = perc > 100 ? 100 : perc

  const trafficLight = val => {
    switch (true) {
      case val > 70:
        return 'green';
      case val > 40:
        return 'orange';
      case val > 0:
        return 'red';
      default:
        return 'none';
    }
  }

  return (
    <div id='progress'>
      <div
        id='bar'
        style={{
          width: `${perc}%`,
          backgroundColor : trafficLight(perc)
        }}>
        <p id='label'>{data}</p>
        </div>
    </div>
  )
}

const Switch = ({children, yes}) => {
  return yes ? children[0] : children[1]
}

// Takes Data and generates a JSX table
// Requires Subject child components
const Directory = ({children, data}) => {

  if(!children) return (<div id="err">No Subjects...</div>)

  // Checks if Subjects are present in data
  const subs = (() => [].concat(children).filter(child => {
    child = [].concat(child.props.info).shift()
    return !!data.find(d => d[child])
  }))()

  // Generate JSX headers from subject props
  const headerBuild = (
    <tr>
      {subs.map((sub, idx) => (
        <th key={`head-${sub}.${idx}`}>{(sub.props.display||sub.props.info)}</th>
      ))}
    </tr>
  )

  // Crawl data object
  const objectCrawler = (path, obj) => {
    path = [].concat(path)
    if(path.length === 1) return obj[path[0]]
    return path.reduce((val, lvl) => val[lvl], obj)
  }

  // Generate JSX table rows from subject props
  // Populates cell input data or child component
  const bodyBuild = data.map((row, idx) => (
    <tr key={`body-${idx}`}>
      {subs.map((item, i) => (
        <td key={`item-${idx}.${i}`}>{
          !item.props.children ?
            objectCrawler(item.props.info ,row)
          : React.cloneElement(item.props.children, {
              data: objectCrawler(item.props.info ,row)
            })
          }
        </td>
      ))}
    </tr>
  ))

  return (
    <div id="Directory">
      <table id='table'>
        <thead>
          {headerBuild}
        </thead>
        <tbody>
          {bodyBuild}
        </tbody>
      </table>
    </div>
  )
}

export {
  Subject,
  Progress,
  Switch
}

export default Directory
