# react-simple-directory

Builds a simple table in react from an array of `Objects`

[![pipeline status](https://gitlab.com/spaceshell_j/react-simple-directory/badges/master/pipeline.svg)](https://gitlab.com/spaceshell_j/react-simple-directory/commits/master)

## Table of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Properties](#Properties)
* [Methods](#Methods)
* [Methods](#Methods)
* [Style](#Methods)
* [Testing](#Testing)
* [Authors](#Authors)

## Installation

To install, you can use [npm](https://npmjs.org/)

```bash
$ npm install react-simple-directory
```


## Usage

Import the component with the following lines.

```JavaScript
import Directory, {
  Subject,
  Progress,
  Switch
} from 'react-simple-directory'
```

Example:

```JavaScript
<Directory data={DATA}>
  <Subject info='name' display='Name'/>
  <Subject info='score' display='Score'>
    <Progress total='100'/>
  <Subject>
</Directory>
```


> A directory must have at least one subject child to display data

### Properties

  |Name|Description|
  |-|-|
  |data|Array of objects to display|

### Methods

Component methods

---

#### Subject

Filters and displays data from the input `data` prop of the Directory component

```JavaScript
<Subject info='score' display='Score'/>
```

Properties:

  |Name|Description|
  |-|-|
  |info| Object key to filter from data |
  |display| String to display as the header title |

---

#### Progress

Displays data from parent `Subject` module as a progress bar

```JavaScript
<Progress total='100'/>
```

  |Name|Description|
  |-|-|
  |total| Maximum value |

---

## Style

> TODO Implement AtomicCss Styling

## Testing

Unit test are run using Jest

```bash
$npm run test
```

## Authors

*James Nicholls*
