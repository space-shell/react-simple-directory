import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import 'jest-enzyme'

import Directory, { Subject, Progress } from './'

import MOCK_DATA from './mock.data'

Enzyme.configure({ adapter: new Adapter() })

describe('Directory Component', () => {

  it('Errors without subjects', () => {
    const wrapper = shallow(<Directory data={MOCK_DATA}/>)
    expect(wrapper.find('#err')).toBePresent()
  })

  it('Checks if Subjects subs are present', () => {
    const wrapper = shallow(
      <Directory data={MOCK_DATA}>
        <Subject/>
      </Directory>
    )
    expect(wrapper.find('#err')).toBeEmpty()
    expect(wrapper.find('#table')).toBePresent()
    expect(wrapper.children().length).not.toBe(0)
  })

  it('Builds Subject headers', () => {
    const wrapper = shallow(
      <Directory data={MOCK_DATA}>
        <Subject info='name' display='Name'/>
      </Directory>
    )
    expect(wrapper.find('th')).toBePresent()
    expect(wrapper.find('th')).toBePresent()
    expect(wrapper.find('th').length).toBe(1)
  })

  it('Skips blank Subjects', () => {
    const wrapper = shallow(
      <Directory data={MOCK_DATA}>
        <Subject info='name' display='Name'/>
        <Subject info='email'/>
        <Subject/>
        <Subject info='id' display='Name'/>
      </Directory>
    )
    expect(wrapper.find('th')).toBePresent()
    expect(wrapper.find('th').length).toBe(3)
    expect(wrapper.find('th').at(0).text()).toEqual('Name')
    expect(wrapper.find('th').at(1).text()).toEqual('email')
    expect(wrapper.find('th').at(2).text()).toEqual('Name')
  })

  it('Builds body items', () => {
    const wrapper = shallow(
      <Directory data={MOCK_DATA}>
        <Subject info='name' display='Name'/>
        <Subject info='lastSeen'/>
        <Subject/>
        <Subject info='pin' display='Name'/>
      </Directory>
    )
    expect(wrapper.find('tr')).toBePresent()
    expect(wrapper.find('tbody')
      .find('tr').length).toBe(MOCK_DATA.length)
  })

  it('Passes data to props', () => {
    const wrapper = shallow(
      <Progress total='100' data='90'/>
    )
    console.log(wrapper.find('#progress'))
    expect(wrapper.find('#progress')).toBePresent()
    expect(wrapper.find('#bar')).toBePresent()
  })
})
